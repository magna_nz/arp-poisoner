README:

* Free to use and distribute
* Only works so far for en0 device on mac (this is one of the wireless adaptors)
* Only support for MAC OSX currently
* Some problems so far with poisoning Windows cache (more testing will happen)
* Requires fping to be installed in /usr/local/sbin directory
  	   	 * Please google to find installation process

For any more information, please contact me at magna.1989@gmail.com
